import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.module.SimpleModule;
import db.model.*;
import org.joda.money.Money;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import serializers.DefaultObjectMapper;
import serializers.MoneyDeserializer;
import serializers.MoneySerializer;

/**
 * abs_accounting
 * Created by BryzzhinIS on 07/02/2018.
 */
public class JacksonTest {

    @Before
    public void setUp() throws Exception {
        Db db = Db.getInstance();
        db.demo1();
    }

    @After
    public void tearDown() {
        Db.getInstance().clear();
    }

    @Test
    public void toJsonTest1() throws JsonProcessingException {
        Db db = Db.getInstance();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
//        module.addDeserializer(Money.class, new MoneyDeserializer());
        module.addSerializer(Money.class, new MoneySerializer());
        mapper.registerModule(module);

//        System.out.println(mapper.writeValueAsString((db.clients.getByI("name", "BANK").peek())));
//        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString((db.clients.getByI("name", "Ivanov Ivan").peek())));
//        System.out.println(mapper.writeValueAsString((db.accounts.getById(1))));
//        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString((db.accounts.getById(2))));
//        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(db.accounts.getRows()));
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(db.documents.getRows()));
//        System.out.println(mapper.writeValueAsString(db.accounts.getRows()));
    }

    @Test
    public void toJsonDocs() throws Exception {
        Db db = Db.getInstance();
        Account acc_cor = db.accounts.getByUI("number", "30102810000000000001");
        Account acc1 = db.accounts.getByUI("number", "40817810000000000001");
        Account acc2 = db.accounts.getByUI("number", "40820810000000000001");
        Document doc1 = db.documents.INSERT(new Document(acc_cor, acc1, Money.parse("RUB 100")));
        Document doc2 = db.documents.INSERT(new Document(acc1, acc2, Money.parse("RUB 10")));
        doc1.docToProv();
//        db.accounts.getRows().stream().sorted(Comparator.comparing(db.model.Account::getAccountNumber)).forEach(System.out::println);

        DefaultObjectMapper mapper = new DefaultObjectMapper();
//        doc2.docToProv();
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(db.documents.getRows()));
    }
}
