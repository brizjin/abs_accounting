import db.engine.Table;
import db.model.*;
import db.model.exceptions.NotEnoughtMoneyException;
import org.joda.money.Money;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;


public class DocumentTest {
    Db db;

    @Before
    public void setUp() throws Exception {
        db = Db.getInstance();
        db.demo1();
    }

    @After
    public void tearDown() throws Exception {
        Db.getInstance().clear();
    }

    @Test
    public void createDocTest() throws Exception {
        Client cl = db.clients.INSERT(new Client("Ivanov"));
        Account acc1 = db.accounts.INSERT(new Account(db.subaccounts.getById(1), cl));
        Account acc2 = db.accounts.INSERT(new Account(db.subaccounts.getById(2), cl));
        Document doc1 = new Document(acc1, acc2, Money.parse("RUB 10"));
        Document doc2 = new Document(acc1, acc2, Money.parse("RUB 10"));
        assert doc1.getId() == null;
        assert doc2.getId() == null;
        System.out.println(doc1);
        System.out.println(doc2);

        Table<Document> docs = new Table<>();
        docs.INSERT(doc1);
        docs.INSERT(doc2);
        System.out.println(doc1);
        System.out.println(doc2);
        assert doc1.getId() == 1;
        assert doc2.getId() == 2;
    }


    @Test(expected = NotEnoughtMoneyException.class)
    public void docToProv1() throws Exception {
        Document doc1 = db.documents.INSERT
                (new Document(
                        db.accounts.getByUI("number", "40817810000000000001"),
                        db.accounts.getByUI("number", "40820810000000000001"),
                        Money.parse("RUB 10")));
        System.out.println(doc1);
        System.out.println(doc1.getAccDT());
        System.out.println(doc1.getAccKT());
        doc1.docToProv();
        System.out.println(doc1);
        System.out.println(doc1.getAccDT());
        System.out.println(doc1.getAccKT());
    }

    @Test
    public void docToProv2() throws Exception {
        Account acc_cor = db.accounts.getByUI("number", "30102810000000000001");
        Account acc1 = db.accounts.getByUI("number", "40817810000000000001");
        Account acc2 = db.accounts.getByUI("number", "40820810000000000001");
        Document doc1 = db.documents.INSERT(new Document(acc_cor, acc1, Money.parse("RUB 100")));
        Document doc2 = db.documents.INSERT(new Document(acc1, acc2, Money.parse("RUB 10")));
//        db.accounts.getRows().stream().sorted(Comparator.comparing(db.model.Account::getAccountNumber)).forEach(System.out::println);
        doc1.docToProv();
        doc2.docToProv();
        doc2.docToProv();

        Optional<BigDecimal> sum_408 = db.accounts.getRows().stream()
                .filter(row -> row.getAccountNumber().substring(0, 3).equals("408"))
                .map(row -> row.getBalans().getAmount())
                .reduce(BigDecimal::add);
        Optional<BigDecimal> sum_301 = db.accounts.getRows().stream()
                .filter(row -> row.getAccountNumber().substring(0, 3).equals("301"))
                .map(row -> row.getBalans().getAmount())
                .reduce(BigDecimal::add);
        if (sum_301.get().compareTo(sum_408.get().multiply(new BigDecimal(-1))) != 0) throw new AssertionError();
    }



    @Test
    public void docToProv3() {
        int threads_count = 24;
        int tasks_count = 10;
        int runs_count = 10;
        int doc_min_sum = 1;
        int doc_max_sum = 100;

        List<Account> accs_40817 = db.accounts.getRows().stream()
                .filter(row -> row.getAccountNumber().substring(0, 3).equals("408"))
                .collect(toList());
//        ConcurrentLinkedQueue<db.model.Account> accs_40817 = db.accounts.getByI("submask", "408");
        Account acc_30102 = db.accounts.getByI("submask", "30102").peek();

        ExecutorService executor = Executors.newFixedThreadPool(threads_count);
        IntStream.range(0, tasks_count).forEach(i -> executor.submit(() -> IntStream.range(0, runs_count).forEach(j -> {
            List<Account> list = new ArrayList<>(accs_40817);
            Collections.shuffle(list);
            int randomDocSum = ThreadLocalRandom.current().nextInt(doc_min_sum, doc_max_sum + 1);
            try {
                db.documents.INSERT(new Document(acc_30102, list.get(0), Money.parse("RUB " + randomDocSum)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        })));

        IntStream.range(0, tasks_count).forEach(i -> executor.submit(() -> IntStream.range(0, runs_count).forEach(j -> {
            List<Account> list = new ArrayList<>(accs_40817);
            Collections.shuffle(list);
            int randomDocSum = ThreadLocalRandom.current().nextInt(doc_min_sum, doc_max_sum + 1);
            try {
                db.documents.INSERT(new Document(list.get(0), list.get(1), Money.parse("RUB " + randomDocSum)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        })));
        Util.stop(executor);

        db.documents.getRows().stream().forEach(System.out::println);
        db.accounts.getRows().stream().forEach(System.out::println);

        if(db.check_balans()!=0) throw new AssertionError();

    }

    @Test
    public void docToProv4() {
        int threads_count = 24;
        int tasks_count = 1000;
        int runs_count = 1000;
        int doc_min_sum = 1;
        int doc_max_sum = 100;


        Util.run(() -> {
            List<Account> list = new ArrayList<>(db.accounts.getRows());
            Collections.shuffle(list);
            int randomDocSum = ThreadLocalRandom.current().nextInt(doc_min_sum, doc_max_sum + 1);
            try {
                Document doc = db.documents.INSERT(new Document(list.get(0), list.get(1), Money.parse("RUB " + randomDocSum)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        },runs_count,tasks_count);

        ExecutorService executor = Executors.newFixedThreadPool(threads_count);
        db.documents.getRows().stream().forEach(doc -> executor.submit(() -> {
            try {
                doc.docToProv();
//                System.out.println("doc=" + doc + ", PROVED");
            } catch (NotEnoughtMoneyException e) {
//                System.out.println("doc=" + doc + ", not enought money for prov");
            }
        }));
        Util.stop(executor);

//        db.documents.getRows().stream().forEach(System.out::println);
//        db.accounts.getRows().stream().forEach(System.out::println);



        if(db.check_balans()!=0) throw new AssertionError();



    }
}