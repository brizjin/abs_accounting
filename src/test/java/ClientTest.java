import db.engine.Table;
import db.model.Client;
import db.model.Db;
import db.model.exceptions.RowAlreadyInTableException;
import db.model.exceptions.UniqueIndexDuplicatedKeyException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClientTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        Db.getInstance().clear();
    }

    @Test
    public void clientsGen() throws Exception {
        Db db = Db.getInstance();
        Client cl1 = db.clients.INSERT(new Client("Ivanov"));
        Client cl2 = db.clients.INSERT(new Client("Petrov"));
        assert cl1.getId() == 1;
        assert cl2.getId() == 2;

    }

    @Test(expected = RowAlreadyInTableException.class)
    public void clientsIndexTest() throws Exception {
        Table<Client> clients = new Table<>();
        clients.addIndex("Name",Client::getName);
        Client cl = new Client("Ivanov");
        clients.INSERT(cl);
        clients.INSERT(cl);
    }

    @Test(expected = UniqueIndexDuplicatedKeyException.class)
    public void clientsUniqueIndexTest() throws Exception {
        Table<Client> clients = new Table<>();
        clients.addUniqueIndex("Name",Client::getName);
        clients.INSERT(new Client("Petrov"));
        clients.INSERT(new Client("Petrov"));

    }

    @Test(expected = UniqueIndexDuplicatedKeyException.class)
    public void clientsUniqueIndexTest2() throws Exception {
        Db db = Db.getInstance();
        db.clients.addUniqueIndex("Name",Client::getName);
        db.clients.INSERT(new Client("Petrov"));
        db.clients.INSERT(new Client("Petrov"));

        Client c = db.clients.getByI("Name","Ivanov2").peek();


    }
}