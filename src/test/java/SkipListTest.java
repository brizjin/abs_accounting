import db.engine.Row;
import db.model.Client;
import db.model.Db;
import db.model.exceptions.RowAlreadyInTableException;
import db.model.exceptions.UniqueIndexDuplicatedKeyException;
import org.junit.After;
import org.junit.Test;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * abs_accounting
 * Created by BryzzhinIS on 08/02/2018.
 */
public class SkipListTest {
    @After
    public void tearDown() {
        Db.getInstance().clear();
    }

    @Test
    public void sortedIndex() throws RowAlreadyInTableException, UniqueIndexDuplicatedKeyException {

        Db db = Db.getInstance();
        db.demo1();
//        db.accounts.getRows().forEach(row -> System.out.println(row));
        db.accounts.getRows("number").navigableKeySet().forEach(System.out::println);
    }

    @Test
    public void skipListTest1() throws RowAlreadyInTableException, UniqueIndexDuplicatedKeyException {

//        ConcurrentSkipListMap<Row,Row> concurrentSkipListMap = new ConcurrentSkipListMap<>((o1, o2) -> 0);
        Comparator<Row> idComparator = Comparator.comparing(Row::getId);
        ConcurrentSkipListMap<Client,Client> concurrentSkipListMap = new ConcurrentSkipListMap<>((o1, o2) -> {
            int comp = o1.getName().compareTo(o2.getName());
            if (comp == 0)
                return idComparator.compare(o1,o2);
            return comp;

        });
        Db db = Db.getInstance();
        Client c  = db.clients.INSERT(new Client("Ivanov7"));
        Client c1 = db.clients.INSERT(new Client("Ivanov8"));
        Client c2 = db.clients.INSERT(new Client("Ivanov2"));
        Client c3 = db.clients.INSERT(new Client("Ivanov3"));
//        Client c4 = db.clients.INSERT(new Client("Ivanov3"));
        Client c5 = db.clients.INSERT(new Client("Ivanov6"));
        concurrentSkipListMap.put(c,c);
        concurrentSkipListMap.put(c1,c1);
        concurrentSkipListMap.put(c2,c2);
        concurrentSkipListMap.put(c3,c3);
//        concurrentSkipListMap.put(c4,c4);
        concurrentSkipListMap.put(c5,c5);

//        System.out.println("ceilingEntry-2: " + concurrentSkipListMap.ceilingEntry("2"));
//        NavigableSet navigableSet = concurrentSkipListMap.descendingKeySet();
        NavigableSet<Client> navigableSet = concurrentSkipListMap.navigableKeySet();
        System.out.println("descendingKeySet: ");
        Iterator<Client> itr = navigableSet.iterator();
        while (itr.hasNext()) {
            Client s = itr.next();
            System.out.println(s);
        }

    }
}
