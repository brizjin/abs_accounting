import db.model.Account;
import db.model.Db;
import db.model.DocumentStatus;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import okhttp3.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;

public class RestServerTest {

    @Before
    public void setUp() throws Exception {
        Db.getInstance().demo1();
        RestServer.main(new String[]{});
    }

    @After
    public void tearDown() {
        Db.getInstance().clear();
    }


    @Test
    public void addDocuments() {
        String add_url = "http://localhost/document/add";
//        RestAssured.given().port(4567).get(add_url).then().statusCode(200);
        RestAssured.given()
                .port(4567)
                .queryParam("amount", "10")
                .queryParam("acc_dt", "30102810000000000001")
                .queryParam("acc_kt", "40820810000000000001")
                .post(add_url)
                .then().statusCode(200);

        Db db = Db.getInstance();
        if (db.documents.getRows().size() != 1) throw new AssertionError();
        System.out.println(db.documents.getRows());
    }

    @Test
    public void addDocuments2() {
        String add_url = "http://localhost/document/add";
//        RestAssured.given().port(4567).get(add_url).then().statusCode(200);
        io.restassured.response.ResponseBody body = RestAssured.given()
                .port(4567)
                .queryParam("amount", "abc")
                .queryParam("acc_dt", "30102810000000000001")
                .queryParam("acc_kt", "40820810000000000001")
                .post(add_url).body();
        Map<Object, Object> map = new JsonPath(body.asString()).getMap("$");
        if (!map.containsKey("Exception")) throw new AssertionError();
    }


    @Test
    public void getDocuments() {
        String list_url = "http://localhost/document";
        RestAssured.given().port(4567).get(list_url).then().statusCode(200);
        JsonPath json = RestAssured.given().port(4567).get(list_url).body().jsonPath();
        List<HashMap> list = json.getList("$");
        if (list.size() > 0) throw new AssertionError();
    }

    @Test
    public void getClientList() {
        JsonPath json = RestAssured.given().port(4567).get("http://localhost/account").body().jsonPath();
        List<HashMap> list = json.getList("$");
        if (list.size() != 5) throw new AssertionError();
    }

    @Test
    public void helloTest() {
        String get = RestAssured.given().port(4567).when().get("http://localhost/hello").body().prettyPrint();//.then().statusCode(200);
        System.out.println(get);
    }

    @Test
    public void genDocuments() {

        int tasks_count = 1000;
        int runs_count = 1000;
        int doc_min_sum = 1;
        int doc_max_sum = 100;

        Db db = Db.getInstance();
        ConcurrentLinkedQueue<String> exceptoin_requests = new ConcurrentLinkedQueue<>();
        ConcurrentLinkedQueue<String> not_enought_money_requests = new ConcurrentLinkedQueue<>();
        ConcurrentLinkedQueue<String> success_requests = new ConcurrentLinkedQueue<>();
        ThreadLocal<OkHttpClient> client = ThreadLocal.withInitial(OkHttpClient::new);
        Util.run(() -> {

            try {
                List<Account> accs = new ArrayList<>(db.accounts.getRows());
                Collections.shuffle(accs);
                Integer randomDocSum = ThreadLocalRandom.current().nextInt(doc_min_sum, doc_max_sum + 1);

                HttpUrl httpUrl = new HttpUrl.Builder()
                        .scheme("http")
                        .host("localhost")
                        .addPathSegment("document")
                        .addPathSegment("add")
                        .port(4567)
                        .addQueryParameter("amount", randomDocSum.toString())
                        .addQueryParameter("acc_dt", accs.get(0).getAccountNumber())
                        .addQueryParameter("acc_kt", accs.get(1).getAccountNumber())
                        .build();
                Request request = new Request.Builder()
                        .url(httpUrl)
                        .post(new FormBody.Builder().build())
                        .build();
                try (Response response = client.get().newCall(request).execute()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                    String body = response.body().string();
//                    System.out.println(body);
                    Map<Object, Object> map = new JsonPath(body).getMap("$");
                    if (map.containsKey("Exception")) {
                        Map<Object, Object> m = (Map<Object, Object>) map.get("Exception");
                        String text = (String) m.get("text");
                        if (text.contains("db.model.exceptions.NotEnoughtMoneyException"))
                            not_enought_money_requests.add(map.toString());
                        else
                            exceptoin_requests.add(map.toString());
                    } else
                        success_requests.add(map.toString());
                }
            } catch (Exception e) {
                exceptoin_requests.add(e.getMessage());
            }
        }, runs_count, tasks_count);

//        db.documents.getRows().forEach(System.out::println);

//        if(exceptoin_requests.size()>0) throw new AssertionError();
        int r_form_docs = not_enought_money_requests.size();
        int r_prov_docs = success_requests.size();
        System.out.println("Success request=" + r_prov_docs + ", not enought money requests=" + r_form_docs + ", errors=" + exceptoin_requests.size());

        long prov_docs = db.documents.getRows().stream().filter(row -> row.getStatus() == DocumentStatus.PROV).count();
        long form_docs = db.documents.getRows().stream().filter(row -> row.getStatus() == DocumentStatus.FORM).count();

        System.out.println("Docs PROV=" + prov_docs + ", docs FORM=" + form_docs);

//        if (r_form_docs != form_docs) throw new AssertionError();
//        if (r_prov_docs != prov_docs) throw new AssertionError();

        if (db.check_balans() != 0) throw new AssertionError();
    }
}
