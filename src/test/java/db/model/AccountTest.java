package db.model;

import db.model.exceptions.RowAlreadyInTableException;
import db.model.exceptions.UniqueIndexDuplicatedKeyException;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * abs_accounting
 * Created by BryzzhinIS on 08/02/2018.
 */
public class AccountTest {

    @Test
    public void accountIndexTest() throws RowAlreadyInTableException, UniqueIndexDuplicatedKeyException {
        Db db = Db.getInstance();
        db.demo1();
        Account acc = db.accounts.getByI("number", ";ldajf;lasjf").peek();
        if (acc == null) {
            System.out.println("123");
        }
        System.out.println(acc);
    }

    @After
    public void tearDown() throws Exception {
        Db.getInstance().clear();
    }
}