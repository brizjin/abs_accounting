package db.model.exceptions;

public class UniqueIndexDuplicatedKeyException extends Exception {
    public UniqueIndexDuplicatedKeyException(String message) {
        super(message);
    }
}
