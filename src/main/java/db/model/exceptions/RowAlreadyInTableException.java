package db.model.exceptions;

public class RowAlreadyInTableException extends Exception {
    public RowAlreadyInTableException(String message) {
        super(message);
    }
}
