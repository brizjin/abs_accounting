package db.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import db.engine.Row;
import db.model.exceptions.NotEnoughtMoneyException;
import org.joda.money.Money;

import java.math.BigDecimal;

public class Document extends Row {

    private final Money amount;
    private Account accDT;
    private Account accKT;
    private DocumentStatus status;

    public Document(Account accDT, Account accKT, Money amount) throws Exception {
        super();
        if (accDT == accKT)
            throw new Exception("db.model.Account DT and KT can not be same");
        this.accDT = accDT;
        this.accKT = accKT;
        this.amount = amount;
        this.status = DocumentStatus.FORM;
    }

    public DocumentStatus getStatus() {
        return status;
    }

    public void docToProv() throws NotEnoughtMoneyException {
        this.writeLock().lock();
        try {
            if (this.status != DocumentStatus.PROV) {
                Account acc1 = accDT.getAccountNumber().compareTo(accKT.getAccountNumber()) <= 0 ? accDT : accKT;
                Account acc2 = accDT.getAccountNumber().compareTo(accKT.getAccountNumber()) <= 0 ? accKT : accDT;

                acc1.writeLock().lock();
                acc2.writeLock().lock();
                try {
                    if (!accDT.getSubMaskNum().equals("30102"))
                        if (accDT.getBalans().minus(amount).getAmount().compareTo(BigDecimal.ZERO) < 0)
                            throw new NotEnoughtMoneyException("docToProv of document " + this + " stoped. db.model.Account " + accDT.getAccountNumber() + " have not enought money");

                    accDT.minusBalans(amount);
                    accKT.plusBalans(amount);
                } finally {
                    acc2.writeLock().unlock();
                    acc1.writeLock().unlock();
                }
                this.status = DocumentStatus.PROV;
            }

        } finally {
            this.writeLock().unlock();
        }
    }

    @JsonProperty("amount")
    public Money getMoneyAmount() {
        return amount;
    }

    @JsonProperty("accDT")
    public String getAccDtAccountNumber() {
        return this.accDT.getAccountNumber();
    }
    @JsonProperty("accKT")
    public String getAccKtAccountNumber() {
        return this.accKT.getAccountNumber();
    }

    @Override
    public String toString() {
        return "db.model.Document{" +
                "id=" + getId() +
                ", accDT=" + accDT.getAccountNumber() +
                ", accKT=" + accKT.getAccountNumber() +
                ", " + amount +
                ", status=" + status +
                '}';
    }

    public Account getAccDT() {
        return accDT;
    }

    public Account getAccKT() {
        return accKT;
    }
}
