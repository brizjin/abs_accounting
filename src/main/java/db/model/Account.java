package db.model;

import com.fasterxml.jackson.annotation.*;
import db.engine.Row;
import org.joda.money.Money;

//@JsonIgnoreProperties({"subMask"})
public class Account extends Row {

    private SubAccount sub;

//    public static ConcurrentHashMap<String, AtomicLong> getAccountNumberCounter() {
//        return accountNumberCounter;
//    }

//    private static final ConcurrentHashMap<String, AtomicLong> accountNumberCounter = new ConcurrentHashMap<>();

    private final String MainNumber;
    private Client client;

    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "amount")
    private volatile Money balans;

    public Account(SubAccount sub, Client client) {
        super();
        this.sub = sub;
        this.MainNumber = this.sub.getSubMask() + "810" + String.format("%012d", this.sub.getCounter().getAndIncrement());
        this.balans = Money.parse("RUB 0");
        this.client = client;
    }

    public String getAccountNumber() {
        return MainNumber;
    }

    @JsonIgnore
    public SubAccount getSubMask() {
        return this.sub;
    }

    public String getSubMaskNum() {
        return this.sub.getSubMask();
    }

    public Money getBalans() {
        return balans;
    }

    public void minusBalans(Money amount) {
        writeLock().lock();
        try {
            this.balans = this.balans.minus(amount);
        } finally {
            writeLock().unlock();
        }
    }

    public void plusBalans(Money amount) {
        writeLock().lock();
        try {
            this.balans = this.balans.plus(amount);
        } finally {
            writeLock().unlock();
        }
    }

    @Override
    public String toString() {
        return "db.model.Account{" +
                "id=" + getId() +
                ", MainNumber='" + MainNumber + '\'' +
                ", balans=" + balans +
                '}';
    }
}
