package db.model;

/**
 * abs_accounting
 * Created by BryzzhinIS on 07/02/2018.
 */
public enum DocumentStatus {FORM, PROV}
