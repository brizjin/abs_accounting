package db.model;

import db.engine.Row;

public class Client extends Row {

    private String Name;

    public Client() {
    }
    public Client(String Name) {
        this.Name = Name;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "db.model.Client{" +
                "id=" + getId() +
                ",Name='" + Name + '\'' +
                '}';
    }
}
