package db.model;

import db.engine.Row;

import java.util.concurrent.atomic.AtomicLong;

public class SubAccount extends Row {
    public String getSubMask() {
        return subMask;
    }

    public AtomicLong getCounter() {
        return counter;
    }

    private final AtomicLong counter;
    private final String subMask;

    public SubAccount(String subMask) {
        this.subMask = subMask;
        counter = new AtomicLong(1);
    }


}
