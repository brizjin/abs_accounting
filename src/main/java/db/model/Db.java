package db.model;

import db.engine.Table;
import db.model.exceptions.RowAlreadyInTableException;
import db.model.exceptions.UniqueIndexDuplicatedKeyException;

import java.math.BigDecimal;
import java.util.Optional;

public class Db {

    public final Table<SubAccount> subaccounts;
    public final Table<Account> accounts;
    public final Table<Document> documents;
    public final Table<Client> clients;

    private Db() {
        accounts = new Table<>();
        documents = new Table<>();
        clients = new Table<>();
        subaccounts = new Table<>();
    }

    private static class DbHolder {
        private final static Db instance = new Db();
    }

    public void clear() {
        accounts.clear();
        documents.clear();
        clients.clear();
        subaccounts.clear();
    }

    public static Db getInstance() {
        return DbHolder.instance;
    }

    public Table<Account> getAccounts() {
        return accounts;
    }

    public void demo1() throws RowAlreadyInTableException, UniqueIndexDuplicatedKeyException {
        clients.addUniqueIndex("name", Client::getName);
        Client bank = clients.INSERT(new Client("BANK"));
        clients.INSERT(new Client("Ivanov Ivan"));
        clients.INSERT(new Client("Petrov Petr"));

//        subaccounts.addUniqueIndex("submask", model.SubAccount::getSubMask);
        SubAccount sub_40817 = subaccounts.INSERT(new SubAccount("40817"));
        SubAccount sub_40820 = subaccounts.INSERT(new SubAccount("40820"));
        SubAccount sub_30102 = subaccounts.INSERT(new SubAccount("30102"));

        accounts.addUniqueIndex("number", Account::getAccountNumber);
        accounts.addIndex("submask", Account::getSubMaskNum);
        accounts.INSERT(new Account(sub_30102, bank));
        accounts.INSERT(new Account(sub_40817, clients.getById(1)));
        accounts.INSERT(new Account(sub_40820, clients.getById(1)));
        accounts.INSERT(new Account(sub_40817, clients.getById(2)));
        accounts.INSERT(new Account(sub_40820, clients.getById(2)));
    }

    private BigDecimal acc_balans_sum(String mask) {
        Optional<BigDecimal> sum = accounts.getRows().stream()
                .filter(row -> row.getAccountNumber().substring(0, 3).equals(mask))
                .map(row -> row.getBalans().getAmount())
                .reduce(BigDecimal::add);
        return sum.get();
    }

    public int check_balans() {
        return acc_balans_sum("301").compareTo(acc_balans_sum("408").multiply(new BigDecimal(-1)));
    }
}
