package db.engine;

import db.model.exceptions.UniqueIndexDuplicatedKeyException;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Function;

public class IndexUnique<T extends Row, R extends Comparable> extends Index<T, R> {

    private final ConcurrentHashMap<R, T> uniqueMap;

    IndexUnique(Table<T> table, Function<T, R> func) {
        super(table, func);
        uniqueMap = new ConcurrentHashMap<>();
    }

    @Override
    void add(T row) throws UniqueIndexDuplicatedKeyException {
        R elementKey = this.getFunc().apply(row);
        //Если индексируемое поле равно null - в индекс не добавляем
        if (elementKey != null) {
            T element = this.uniqueMap.get(elementKey);
            if (element == null) {
                uniqueMap.put(elementKey, row);
            } else {
                throw new UniqueIndexDuplicatedKeyException("on adding " + row + " unique index violated on key='" + elementKey + "'");
            }
        }
        this.getSortedMap().put(row, row);
    }

    @Override
    ConcurrentLinkedQueue<T> getByIndex(R findElement) {
        ConcurrentLinkedQueue<T> list = new ConcurrentLinkedQueue<>();
        T ref = uniqueMap.get(findElement);
        if (ref != null) {
            list.add(ref);
        }
        return list;
//        return null;
    }

    T getElement(R findElement) {
        return uniqueMap.get(findElement);
    }

    @Override
    public void clear() {
        super.clear();
        uniqueMap.clear();
    }

}
