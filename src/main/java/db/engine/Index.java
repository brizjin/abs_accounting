package db.engine;

import db.model.Client;
import db.model.exceptions.UniqueIndexDuplicatedKeyException;

import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Function;

public class Index<T extends Row,R extends Comparable> {

    private final ConcurrentHashMap<R, ConcurrentLinkedQueue<T>> map;

    public ConcurrentSkipListMap<T, T> getSortedMap() {
        return sortedMap;
    }

    private final ConcurrentSkipListMap<T,T> sortedMap;
    private final Function<T, R> func;
    private final Table<T> table;

    Index(Table<T> table, Function<T, R> func) {
        this.func = func;
        this.map = new ConcurrentHashMap<>();
        this.table = table;

        Comparator<T> idComparator = Comparator.comparing(T::getId);
        this.sortedMap = new ConcurrentSkipListMap<>((o1, o2) -> {
            R key1 = func.apply(o1);
            R key2 = func.apply(o2);
            if(key1 == null && key2 != null) return -1;
            if(key1 != null && key2 == null) return 1;
            if(key1 == null) return 1;
            int comp = key1.compareTo(key2);
            if (comp == 0)
                return idComparator.compare(o1,o2);
            return comp;
        });
    }

//    public ConcurrentHashMap<R, ConcurrentLinkedQueue<T>> getMap() {
//        return map;
//    }

    public Function<T, R> getFunc() {
        return func;
    }

//    public Table<T> getTable() {
//        return table;
//    }

    void add(T row) throws UniqueIndexDuplicatedKeyException {
        R elementKey = this.func.apply(row);
        if (elementKey != null) {
            ConcurrentLinkedQueue<T> list = map.get(elementKey);
            if (list == null) {
//                System.out.println("NEW ELEMENT LIST");
                list = new ConcurrentLinkedQueue<>();
                map.put(elementKey, list);
            }
            list.add(row);
        }
        sortedMap.put(row, row);
    }

//    void rebuild() throws Exception {
//        sortedMap.clear();
//        map.clear();
//        for (T tRef : this.table.getRows()) {
//            add(tRef);
//        }
//    }

    ConcurrentLinkedQueue<T> getByIndex(R findElement) {
        return map.get(findElement);
    }

    public void clear() {
        this.map.clear();
        this.sortedMap.clear();
    }
}
