package db.engine;

import db.model.exceptions.RowAlreadyInTableException;
import db.model.exceptions.UniqueIndexDuplicatedKeyException;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

public class Table<T extends Row> {
    //    private final ConcurrentLinkedQueue<T> rows;
//    private final ConcurrentHashMap<T, T> rows;
    private final AtomicLong idCounter = new AtomicLong(1);
    private final IndexUnique<T, Long> indexID;
    private final ConcurrentHashMap<String, Index> indexes;


    public Table() {
//        this.rows = new ConcurrentHashMap<>();
        this.indexes = new ConcurrentHashMap<>();
        indexID = new IndexUnique<>(this, T::getId);

    }

    public T INSERT(T row) throws RowAlreadyInTableException, UniqueIndexDuplicatedKeyException {
        if (this.indexID.getSortedMap().get(row) != null)
            throw new RowAlreadyInTableException("db.engine.Row already in table " + row.getClass().getName());
        row.setId(idCounter.getAndIncrement());
        this.indexID.getSortedMap().put(row, row);
        for (Map.Entry<String, Index> entry : indexes.entrySet())
            entry.getValue().add(row);
        indexID.add(row);
        return row;
    }

    public void clear() {
        indexID.clear();
        for (Map.Entry<String, Index> entry : indexes.entrySet()) {
            entry.getValue().clear();
        }
        idCounter.set(1);
    }

    public <V extends Comparable> void addIndex(String indexName, Function<T, V> func) {
        indexes.put(indexName, new Index<>(this, func));
    }

    public <V> void addUniqueIndex(String indexName, Function<T, V> func) {
        indexes.put(indexName, new IndexUnique(this, func));
    }

    public <V extends Comparable> ConcurrentLinkedQueue<T> getByI(String indexName, V findString) {
        Index<T, V> index = indexes.get(indexName);
        if (index != null) {
            return index.getByIndex(findString);
        }
        return new ConcurrentLinkedQueue<>();
    }

    public <V extends Comparable> T getByUI(String indexName, V findString) {
        IndexUnique<T, V> index = (IndexUnique<T, V>) indexes.get(indexName);
        return index.getElement(findString);
    }


    public Collection<T> getRows() {
        return this.indexID.getSortedMap().values();
    }
    public ConcurrentSkipListMap getRows(String IndexName) {
        return indexes.get(IndexName).getSortedMap();
    }

    public T getById(Long id) {
        return indexID.getElement(id);
    }

    public T getById(String id) {
        return indexID.getElement(Long.parseLong(id));
    }

    public T getById(int id) {
        return indexID.getElement((long) id);
    }

}