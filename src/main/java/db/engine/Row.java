package db.engine;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Row {

    private Long id;

    public ReentrantReadWriteLock.WriteLock writeLock() {
        return lock.writeLock();
    }
    public ReentrantReadWriteLock.ReadLock readLock() {
        return lock.readLock();
    }

    final ReentrantReadWriteLock lock;

    protected Row() {
        lock = new ReentrantReadWriteLock();
    }

    public String toString() {
        return String.format("Row of %s [id=%s]", getClass().getName(), this.id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
