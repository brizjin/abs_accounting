import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Util {
    public static void stop(ExecutorService executor) {
        try {
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            System.err.println("termination interrupted");
        } finally {
            if (!executor.isTerminated()) {
                System.err.println("killing non-finished tasks");
            }
            executor.shutdownNow();
        }
    }

    public static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    static void run(Runnable task, int runs_count, int tasks_count) {
        int threads_count = 24;
        ExecutorService executor = Executors.newFixedThreadPool(threads_count);

        IntStream.range(0, tasks_count).forEach(i -> {
            executor.submit(() -> IntStream.range(0, runs_count).forEach(j -> task.run()));
        });
        Util.stop(executor);
    }
}
