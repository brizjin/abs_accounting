package serializers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.money.Money;

/**
 * abs_accounting
 * Created by BryzzhinIS on 07/02/2018.
 */
public class DefaultObjectMapper extends ObjectMapper {
    public DefaultObjectMapper() {
        super();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Money.class, new MoneyDeserializer());
        module.addSerializer(Money.class, new MoneySerializer());
        module.addSerializer(Exception.class, new ExceptionSerializer());
        this.registerModule(module);
    }
}
