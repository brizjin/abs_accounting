package serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.joda.money.Money;

import java.io.IOException;

/**
 * abs_accounting
 * Created by BryzzhinIS on 07/02/2018.
 */
public class ExceptionSerializer extends StdSerializer<Exception> {
    public ExceptionSerializer() {
        super(Exception.class);
    }

    @Override
    public void serialize(Exception value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeStartObject();
        jgen.writeObjectFieldStart("Exception");
//        jgen.writeString(value.toString());
//        jgen.writeNumberField("Exception", value.id);
        jgen.writeObjectField("text", value.toString());
        jgen.writeObjectField("stacktrace", value.getStackTrace());
//        jgen.writeNumberField("owner", value.owner.id);
        jgen.writeEndObject();

        jgen.writeEndObject();

    }
}
