import db.model.Account;
import db.model.Db;
import db.model.Document;
import db.model.exceptions.RowAlreadyInTableException;
import db.model.exceptions.UniqueIndexDuplicatedKeyException;
import org.joda.money.Money;
import serializers.DefaultObjectMapper;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.init;
import static spark.debug.DebugScreen.enableDebugScreen;

public class RestServer {
    public static class MapperHolder {
        private static final ThreadLocal<DefaultObjectMapper> mapper = ThreadLocal.withInitial(DefaultObjectMapper::new);
        static DefaultObjectMapper getInstance() {
            return mapper.get();
        }
    }
    public static void main(String[] args) throws RowAlreadyInTableException, UniqueIndexDuplicatedKeyException {
        Db db = Db.getInstance();
//        db.demo1();
        get("/hello", (req, res) -> "Hello World");
        get("/client", (req, res) -> MapperHolder.getInstance().writeValueAsString(db.clients.getRows()));
        get("/account", (req, res) -> MapperHolder.getInstance().writeValueAsString(db.accounts.getRows()));
        get("/document", (req, res) -> MapperHolder.getInstance().writeValueAsString(db.documents.getRows()));
        get("/client/:id", (req, res) -> MapperHolder.getInstance().writeValueAsString(db.clients.getById(1)));
        get("/account/:id", (req, res) -> MapperHolder.getInstance().writeValueAsString(db.accounts.getById(1)));
        get("/document/:id", (req, res) -> MapperHolder.getInstance().writeValueAsString(db.documents.getById(1)));

        post("/document/add", (request, response) -> {
            Object return_obj = null;
            try {
                String p_amount = request.queryParams("amount");
                String p_acc_dt = request.queryParams("acc_dt");
                String p_acc_kt = request.queryParams("acc_kt");
//                System.out.println("p_amount=" + p_amount);
//                System.out.println("p_acc_dt=" + p_acc_dt);
//                System.out.println("p_acc_kt=" + p_acc_kt);
                Account acc_dt = db.accounts.getByI("number", p_acc_dt).peek();
                Account acc_kt = db.accounts.getByI("number", p_acc_kt).peek();
                if (acc_dt == null) throw new Exception("Can not find acc_dt by account number[" + p_acc_dt + "]");
                if (acc_kt == null) throw new Exception("Can not find acc_dt by account number[" + p_acc_dt + "]");
                Money amount = null;
                try {
                    amount = Money.parse("RUB " + p_amount);
                } catch (Exception e) {
                    throw new Exception("Incorrect document amount [" + p_amount + "]");
                }

                Document doc = db.documents.INSERT(new Document(acc_dt, acc_kt, amount));
                doc.docToProv();
                return_obj = doc;
            } catch (Exception e) {
                return_obj = e;
            }
            return MapperHolder.getInstance().writeValueAsString(return_obj);
        });
        enableDebugScreen();
        init();
    }
}
